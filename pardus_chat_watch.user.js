// ==UserScript==
// @name        Pardus Chat Watch
// @namespace   http://userscripts.xcom-alliance.info/
// @description Monitor General chat or Alliance Chat from the navigation screen
// @version     1.3
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://chat.pardus.at/chattext.php*#chatwatcher
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/logout.php
// @updateURL 	http://userscripts.xcom-alliance.info/chat_watch/pardus_chat_watch.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/chat_watch/pardus_chat_watch.user.js
// @resource	settingsIcon settings_icon.gif
// @icon 		http://userscripts.xcom-alliance.info/chat_watch/icon.png
// @grant       GM_setValue
// @grant       GM_getValue
// @grant       GM_getResourceURL
// @grant       GM_xmlhttpRequest
// ==/UserScript==

// v1.3 (22-12-2014) - added ability to show in the Logout screen

var showOnLogoutScreen = true;
var showOnNavigationScreen = false;

// return the universe we are in
function getUniverse() {
	var universe = window.location.host.substr(0, window.location.host.indexOf('.'));
	if (universe.toLowerCase() === 'chat') {
		universe = location.search.substr(location.search.indexOf('uni=') + 4);
	}
	return universe.charAt(0).toUpperCase() + universe.substring(1);
};

// get the channel
function getChannel() {
	return GM_getValue(getUniverse() + '_Channel', 'ally');
};

// set the channel
function setChannel(channel) {
	GM_setValue(getUniverse() + '_Channel', channel);
};

// get the chat text colour
function getChatColor() {
	return GM_getValue(getUniverse() + '_ChatColor', 'FFFFFF');
};

// set the chat text color
function setChatColor(color) {
	GM_setValue(getUniverse() + '_ChatColor', color);
};

// get the toggle state
function getToggleState() {
	return GM_getValue(getUniverse() + '_ToggleState', 'hide');
};

// set the toggle state
function setToggleState(state) {
	GM_setValue(getUniverse() + '_ToggleState', state);
};

// add css to the page (based on which page we are decorating)
function addCSSToPage(pageName) {
	var css = '/* this css has been injected in to the page from a greasemonkey script */'+ '\n';

	if (pageName == 'chattext') {

		css += ' body { font-family: Verdana,Arial,sans-serif; background: black!important; margin: 0 0 30px 0; } ';
		css += ' span, a, div span a, a:link { font-size: 11px; } ';
		css += ' div span img { height: 13px; } ';
		css += ' span.date { font-size: 8px; } ';
		css += ' a[target=_blank], .date a { display: none; } ';
		css += ' #ChatWnd { margin: 0 5px; } ';
		css += ' #ChatWnd > div { line-height: 13px; } ';
		css += ' #ChatButtonHolder { margin-top: 3px; border-top: 1px solid #999; padding: 4px; background-color:#333; position: fixed; bottom: 0; width: 572px; }';
		css += ' #ChatTextInput { border: 1px solid #ccc; background-color: #000; color: #fff; padding: 2px 4px; font-size: 10px; width: 380px; margin-left: 5px; }';
		css += ' #ChatMsgColor { width: 100px; }';
		css += ' #ChatTextButton { cursor: pointer; background-color: #000; color: #fff; border: 1px solid #fff; padding: 3px 10px 2px 10px; margin-left: 10px; float: right; }';

	} else if (pageName == 'main') {

		css += ' #ChatFrame { width: 580px; height: 150px; border: 3px double #999; padding: 2px 0 0; }';
		css += ' #ButtonHolder { width: 580px; margin-top: 3px; }';
		css += ' #TextEmotion { float: left; color: #FFF; }';
		css += ' #ButtonGC, #ButtonAC { cursor: pointer; margin: 0 5px; color: #999; padding: 5px 10px 2px; }';
		css += ' #ButtonGC.selected, #ButtonAC.selected { cursor: default; background-color: #000; color: #fff; border: 1px solid #999; border-top: none; }';
		css += ' #showHideIcon { float: right; cursor: pointer; }';

	}

	var CSS = document.createElement("style");
	CSS.setAttribute("type", "text/css");
	CSS.innerHTML = css;
	document.body.appendChild(CSS);
};

if ((showOnNavigationScreen === true && document.URL.search('pardus.at/main.php') > -1) || (showOnLogoutScreen === true && document.URL.search('pardus.at/logout.php') > -1)) {

	if (document.getElementsByTagName('TABLE').length) {

		// get the url to use for launching chattext.php
		function getChatTextURL() {
			return location.protocol + '//chat.pardus.at/chattext.php?channel=' + getChannel() + '&amp;uni=' + getUniverse() + '#chatwatcher';
		};

		// show/hide the chat iframe and buttons
		function showHideToggle() {
			var toggleState = getToggleState();
			var action = '';
			if (toggleState === 'show') {
				setToggleState('hide');
				action = 'none';
			} else {
				setToggleState('show');
			}
			document.getElementById('ChatFrame').style.display = action;
			document.getElementById('ChatFrame').src = getChatTextURL();
			document.getElementById('TextEmotion').style.display = action;
			document.getElementById('ButtonGC').style.display = action;
			document.getElementById('ButtonAC').style.display = action;
		};

		// add our css to the page
		addCSSToPage('main');
		
		var insertPoint = null;
		if (document.URL.search('pardus.at/main.php') > -1) {
			// force our appended content to be centered
			insertPoint = document.getElementsByTagName('TABLE')[0].rows[0].cells[1];
			insertPoint.setAttribute('align','center');
		} else 
		if (document.URL.search('pardus.at/logout.php') > -1) {
			var _table = document.getElementsByTagName('TABLE')[0];
			insertPoint = _table.rows[_table.rows.length-2].cells[1];
			insertPoint.setAttribute('align','center');
		}

		// chat button wrapper
		var divEl = document.createElement('div');
		divEl.id = 'ButtonHolder';

		// iframe for chat text
		var iframe = document.createElement('iframe');
		iframe.name = 'ChatFrame';
		iframe.id = 'ChatFrame';
		if (getToggleState() === 'hide') {
			iframe.style.display = 'none';
		}
		iframe.src = getChatTextURL();

		// append the chat text iframe
		insertPoint.appendChild(iframe);

		// text for /me
		var txtEl = document.createElement('span');
		txtEl.id = 'TextEmotion';
		if (getToggleState() === 'hide') {
			txtEl.style.display = 'none';
		}
		txtEl.innerHTML = '<em>Start your chat with <strong>/me</strong> for emotion</em>';
		
		// general chat tab
		var btn1El = document.createElement('span');
		btn1El.id = 'ButtonGC';
		if (getToggleState() === 'hide') {
			btn1El.style.display = 'none';
		}
		if (getChannel() === 'general') btn1El.className = 'selected';
		btn1El.innerHTML = 'General Chat';
		btn1El.addEventListener('click', function(e) {
			if (e.currentTarget.className === 'selected') return;
			e.currentTarget.className = 'selected';
			document.getElementById('ButtonAC').className = '';
			setChannel('general');
			document.getElementById('ChatFrame').src = getChatTextURL();
		});

		// alliance chat tab
		var btn2El = document.createElement('span');
		btn2El.id = 'ButtonAC';
		if (getToggleState() === 'hide') {
			btn2El.style.display = 'none';
		}
		if (getChannel() === 'ally') btn2El.className = 'selected';
		btn2El.innerHTML = 'Alliance Chat';
		btn2El.addEventListener('click', function(e) {
			if (e.currentTarget.className === 'selected') return;
			e.currentTarget.className = 'selected';
			document.getElementById('ButtonGC').className = '';
			setChannel('ally');
			document.getElementById('ChatFrame').src = getChatTextURL();
		});

		// show/hide icon
 		var showHideEl = document.createElement('img');
 		showHideEl.id = 'showHideIcon';
 		showHideEl.title = 'Show/Hide Pardus Chat Watch';
 		showHideEl.src = GM_getResourceURL('settingsIcon');
 		showHideEl.addEventListener('click', function(e) {
 			showHideToggle();
 		});

		// bring everything together
		divEl.appendChild(txtEl);
		divEl.appendChild(btn1El);
		divEl.appendChild(btn2El);
  		divEl.appendChild(showHideEl);

		// and add to the page
		insertPoint.appendChild(divEl);

	}

} else
if (document.URL.search('chat.pardus.at/chattext.php') > -1) {

	// injected to the page to over-ride the built-in callback
	function _ajaxCallback(result, errors) {
		scroll = (result && result["scroll"]) ? true : false;
		if (!result) return;
		else if (result["first"] > lastMsg) {
			lastMsg = result["last"];
			chat.innerHTML += decodeString(result["new"]);
			window.setTimeout('window.scrollBy(0, 999999)', 50);
		}
	};
	
	// inject our replacement callback function and over-ride the built-in callback
	var injectJS = _ajaxCallback.toString();
	injectJS += 'window.ajaxCallback = _ajaxCallback;';
	var JS = document.createElement("script");
	JS.setAttribute("type", "text/javascript");
	JS.innerHTML = injectJS;
	document.body.appendChild(JS);
	
	// add our css to the page
	addCSSToPage('chattext');

	// GM code to sent chat text to the server
	function sendDataToServer(txt) {
		var chatmsg_text = txt.replace(/\\/g, '\\\\').replace(/\'/g, '\\\'').replace(/€/g, '&#8364;');
		var chatmsg_type = 'N';
		if (chatmsg_text.indexOf('/me ') === 0) {
			chatmsg_text = chatmsg_text.substr(4);
			chatmsg_type = 'E';
		}
		var params = 'channel=' + getChannel();
		params += '&action=send';
		params += '&lastMsg=' + unsafeWindow.lastMsg;
		params += '&chatmsg_text=' + chatmsg_text;
		params += '&chatmsg_color=' + getChatColor();
		params += '&chatmsg_type=' + chatmsg_type;
		params += '&uni=' + getUniverse();
		GM_xmlhttpRequest({
			method: "POST",
			url: 'chattext.php',
			data: params,
			headers: {
				"Content-Type": "application/x-www-form-urlencoded",
				"Content-Length": params.length,
				"Connection": 'close'
			}
		});
	};

	// chat text input
	var chatEl = document.createElement('input');
	chatEl.id = 'ChatTextInput';
	chatEl.addEventListener('keypress', function(e) {
		var key = 0;
		if (window.event) {
			key = e.keyCode;
		} else if (e.which) {
			key = e.which;
		}
		if (key == 13) {
			document.getElementById('ChatTextButton').click();
			var txt = document.getElementById('ChatTextInput').value;
			document.getElementById('ChatTextInput').value = '';
			if (txt !== '') {
				sendDataToServer(txt);
			}
		}
	});

	// chat colour select
	var chatColor = document.createElement('select');
	chatColor.id = 'ChatMsgColor';
	var chatMsgColor = getChatColor();
	chatColor.innerHTML = '' + 
		'<optgroup label="Non Premium">' + 
		'<option value="FFFFFF" style="color:#FFFFFF;"' + (chatMsgColor === 'FFFFFF' ? ' selected="selected"' : '') + '>White</option>' + 
		'<option value="D3D3D3" style="color:#D3D3D3;"' + (chatMsgColor === 'D3D3D3' ? ' selected="selected"' : '') + '>Light Gray</option>' + 
		'<option value="AEAEAE" style="color:#AEAEAE;"' + (chatMsgColor === 'AEAEAE' ? ' selected="selected"' : '') + '>Gray</option>' + 
		'<option value="F5DEB3" style="color:#F5DEB3;"' + (chatMsgColor === 'F5DEB3' ? ' selected="selected"' : '') + '>Wheat</option>' + 
		'<option value="E69600" style="color:#E69600;"' + (chatMsgColor === 'E69600' ? ' selected="selected"' : '') + '>Light Brown</option>' + 
		'<option value="FF4500" style="color:#FF4500;"' + (chatMsgColor === 'FF4500' ? ' selected="selected"' : '') + '>Light Red</option>' + 
		'<option value="FF8540" style="color:#FF8540;"' + (chatMsgColor === 'FF8540' ? ' selected="selected"' : '') + '>Orange</option>' + 
		'<option value="FFFF00" style="color:#FFFF00;"' + (chatMsgColor === 'FFFF00' ? ' selected="selected"' : '') + '>Yellow</option>' + 
		'<option value="98FB98" style="color:#98FB98;"' + (chatMsgColor === '98FB98' ? ' selected="selected"' : '') + '>Pale Green</option>' + 
		'<option value="00FF00" style="color:#00FF00;"' + (chatMsgColor === '00FF00' ? ' selected="selected"' : '') + '>Green</option>' + 
		'<option value="28C928" style="color:#28C928;"' + (chatMsgColor === '28C928' ? ' selected="selected"' : '') + '>Dark Green</option>' + 
		'<option value="1E90FF" style="color:#1E90FF;"' + (chatMsgColor === '1E90FF' ? ' selected="selected"' : '') + '>Blue</option>' + 
		'<option value="00FFFF" style="color:#00FFFF;"' + (chatMsgColor === '00FFFF' ? ' selected="selected"' : '') + '>Cyan</option>' + 
		'<option value="C0D0FF" style="color:#C0D0FF;"' + (chatMsgColor === 'C0D0FF' ? ' selected="selected"' : '') + '>Light Blue</option>' + 
		'<option value="FFB4DC" style="color:#FFB4DC;"' + (chatMsgColor === 'FFB4DC' ? ' selected="selected"' : '') + '>Pink</option>' + 
		'<option value="FF69B4" style="color:#FF69B4;"' + (chatMsgColor === 'FF69B4' ? ' selected="selected"' : '') + '>Hot Pink</option>' + 
		'<option value="EE82EE" style="color:#EE82EE;"' + (chatMsgColor === 'EE82EE' ? ' selected="selected"' : '') + '>Violet</option>' + 
		'</optgroup>' + 
		'<optgroup label="Premium Only">' + 
		'<option value="EDE275" style="color:#EDE275;"' + (chatMsgColor === 'EDE275' ? ' selected="selected"' : '') + '>Khaki</option>' + 
		'<option value="F87217" style="color:#F87217;"' + (chatMsgColor === 'F87217' ? ' selected="selected"' : '') + '>Dark Orange</option>' + 
		'<option value="FFD700" style="color:#FFD700;"' + (chatMsgColor === 'FFD700' ? ' selected="selected"' : '') + '>Gold</option>' + 
		'<option value="B1FB17" style="color:#B1FB17;"' + (chatMsgColor === 'B1FB17' ? ' selected="selected"' : '') + '>Green Yellow</option>' + 
		'<option value="5EFB6E" style="color:#5EFB6E;"' + (chatMsgColor === '5EFB6E' ? ' selected="selected"' : '') + '>Spring Green</option>' + 
		'<option value="87CEEB" style="color:#87CEEB;"' + (chatMsgColor === '87CEEB' ? ' selected="selected"' : '') + '>Sky Blue</option>' + 
		'<option value="F9966B" style="color:#F9966B;"' + (chatMsgColor === 'F9966B' ? ' selected="selected"' : '') + '>Salmon</option>' + 
		'<option value="FC3399" style="color:#FC3399;"' + (chatMsgColor === 'FC3399' ? ' selected="selected"' : '') + '>Light Purple</option>' + 
		'</optgroup>';
	chatColor.addEventListener('change', function(e) {
		setChatColor(e.currentTarget.value);
	});

	// chat text send button
	var chatBtn = document.createElement('span');
	chatBtn.id = 'ChatTextButton';
	chatBtn.innerHTML = 'Send';
	chatBtn.addEventListener('click', function(e) {
		var txt = document.getElementById('ChatTextInput').value;
		document.getElementById('ChatTextInput').value = '';
		if (txt !== '') {
			sendDataToServer(txt);
		}
	});

	// chat button wrapper
	var divEl = document.createElement('div');
	divEl.id = 'ChatButtonHolder';
	divEl.appendChild(chatColor);
	divEl.appendChild(chatEl);
	divEl.appendChild(chatBtn);

	// append the chat button wrapper to the body
	document.body.appendChild(divEl);

}
