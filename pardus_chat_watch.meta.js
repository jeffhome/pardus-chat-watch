// ==UserScript==
// @name        Pardus Chat Watch
// @namespace   http://userscripts.xcom-alliance.info/
// @description Monitor General chat or Alliance Chat from the navigation screen
// @version     1.3
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://chat.pardus.at/chattext.php*#chatwatcher
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/logout.php
// @updateURL 	http://userscripts.xcom-alliance.info/chat_watch/pardus_chat_watch.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/chat_watch/pardus_chat_watch.user.js
// @resource	settingsIcon settings_icon.gif
// @icon 		http://userscripts.xcom-alliance.info/chat_watch/icon.png
// @grant       GM_setValue
// @grant       GM_getValue
// @grant       GM_getResourceURL
// @grant       GM_xmlhttpRequest
// ==/UserScript==